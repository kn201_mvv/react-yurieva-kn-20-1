import { useState } from "react";
import TR from "./TR";
import { Table, TableCell, TableHead, TableRow } from "@mui/material";

export default function Cart() {
    const prod = [
        { id: 0, name: "Cousin", price: 100, min: 0 },
        { id: 1, name: "Slave", price: 150, min: 0 }
    ]
    const [count, SetCount] = useState(prod.length);
    const [total, SetTotal] = useState(prod[0].price + prod[1].price);

    const SetCountHNDL = (int) => {
        SetCount(count + int);
    }
    const SetTotalHNDL = (int) => {
        SetTotal(total + int);
    }
    return (
        <>
            <h1>CART</h1>
            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow><TableCell>Name</TableCell><TableCell>Price</TableCell><TableCell>Quantity</TableCell><TableCell>Total</TableCell></TableRow>
                </TableHead>
                {
                    prod.map(pro =>
                        <TR key={pro.id} name={pro.name} price={pro.price} min={pro.min} setCount={SetCountHNDL} setTotal={SetTotalHNDL} />
                    )
                }
                <TableHead><TableRow><TableCell colSpan={2}>Total</TableCell><TableCell>{count}</TableCell><TableCell>{total}</TableCell>  </TableRow>          </TableHead>
            </Table>
        </>
    );

}