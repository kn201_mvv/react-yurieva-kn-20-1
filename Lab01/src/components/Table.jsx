function Table() {
    return (
        <table>
            <tr>
                <td><b>First Name</b></td>
                <td>John</td>
            </tr>
            <tr>
                <td><b>Last Name</b></td>
                <td>Silver</td>
            </tr>
            <tr>
                <td><b>Occupation</b></td>
                <td>Pirate Captain</td>
            </tr>
        </table>
    )
}

export default Table;