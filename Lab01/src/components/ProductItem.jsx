function ProductItem(props) {
    return (
        <>
            <li>
                <ul>
                    <li><b>{props.item.name} ({props.item.id})</b></li>
                    <li>Price: {props.item.price} USD</li>
                    <li>Availability: {props.item.available}</li>
                </ul>
            </li>
        </>
    )
}
export default ProductItem